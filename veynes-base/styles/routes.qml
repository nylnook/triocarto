<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis simplifyDrawingHints="1" labelsEnabled="1" simplifyMaxScale="1" hasScaleBasedVisibilityFlag="0" simplifyAlgorithm="0" simplifyDrawingTol="1" readOnly="0" version="3.18.1-Zürich" styleCategories="AllStyleCategories" simplifyLocal="1" minScale="100000000" maxScale="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal startField="" endField="" mode="0" fixedDuration="0" enabled="0" durationField="" accumulate="0" startExpression="" endExpression="" durationUnit="min">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <renderer-v2 forceraster="0" enableorderby="0" type="RuleRenderer" symbollevels="0">
    <rules key="{8f56e1a0-6c24-46f8-a6a1-60f353fde6ec}">
      <rule filter=" &quot;highway&quot; IN ( 'motorway' , 'motorway_link', 'primary','primary_link')" key="{270cea47-8d00-40a5-b2ff-d09042d17397}" label="autoroutes et nationales" symbol="0"/>
      <rule filter="&quot;highway&quot; IN ('secondary', 'secondary_link')" key="{51266df3-8994-40fa-899d-a62211050c5b}" label="départementales" symbol="1"/>
      <rule filter="&quot;highway&quot; IN ('track', 'path')" key="{34ea0bd3-e14a-4f75-b40e-b838554c84a2}" label="pistes et chemins" symbol="2"/>
      <rule filter="ELSE" key="{7e81712d-fff2-4c58-a913-d1b348f0683a}" label="autres routes" symbol="3"/>
    </rules>
    <symbols>
      <symbol name="0" type="line" alpha="1" force_rhr="0" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" class="SimpleLine" enabled="1" pass="0">
          <Option type="Map">
            <Option name="align_dash_pattern" type="QString" value="0"/>
            <Option name="capstyle" type="QString" value="square"/>
            <Option name="customdash" type="QString" value="5;2"/>
            <Option name="customdash_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="customdash_unit" type="QString" value="MM"/>
            <Option name="dash_pattern_offset" type="QString" value="0"/>
            <Option name="dash_pattern_offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="dash_pattern_offset_unit" type="QString" value="MM"/>
            <Option name="draw_inside_polygon" type="QString" value="0"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="line_color" type="QString" value="35,35,35,255"/>
            <Option name="line_style" type="QString" value="solid"/>
            <Option name="line_width" type="QString" value="14"/>
            <Option name="line_width_unit" type="QString" value="MapUnit"/>
            <Option name="offset" type="QString" value="0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="ring_filter" type="QString" value="0"/>
            <Option name="tweak_dash_pattern_on_corners" type="QString" value="0"/>
            <Option name="use_custom_dash" type="QString" value="0"/>
            <Option name="width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
          </Option>
          <prop v="0" k="align_dash_pattern"/>
          <prop v="square" k="capstyle"/>
          <prop v="5;2" k="customdash"/>
          <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
          <prop v="MM" k="customdash_unit"/>
          <prop v="0" k="dash_pattern_offset"/>
          <prop v="3x:0,0,0,0,0,0" k="dash_pattern_offset_map_unit_scale"/>
          <prop v="MM" k="dash_pattern_offset_unit"/>
          <prop v="0" k="draw_inside_polygon"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="35,35,35,255" k="line_color"/>
          <prop v="solid" k="line_style"/>
          <prop v="14" k="line_width"/>
          <prop v="MapUnit" k="line_width_unit"/>
          <prop v="0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0" k="ring_filter"/>
          <prop v="0" k="tweak_dash_pattern_on_corners"/>
          <prop v="0" k="use_custom_dash"/>
          <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
        <layer locked="0" class="SimpleLine" enabled="1" pass="0">
          <Option type="Map">
            <Option name="align_dash_pattern" type="QString" value="0"/>
            <Option name="capstyle" type="QString" value="square"/>
            <Option name="customdash" type="QString" value="5;2"/>
            <Option name="customdash_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="customdash_unit" type="QString" value="MM"/>
            <Option name="dash_pattern_offset" type="QString" value="0"/>
            <Option name="dash_pattern_offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="dash_pattern_offset_unit" type="QString" value="MM"/>
            <Option name="draw_inside_polygon" type="QString" value="0"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="line_color" type="QString" value="227,26,28,255"/>
            <Option name="line_style" type="QString" value="solid"/>
            <Option name="line_width" type="QString" value="12"/>
            <Option name="line_width_unit" type="QString" value="MapUnit"/>
            <Option name="offset" type="QString" value="0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="ring_filter" type="QString" value="0"/>
            <Option name="tweak_dash_pattern_on_corners" type="QString" value="0"/>
            <Option name="use_custom_dash" type="QString" value="0"/>
            <Option name="width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
          </Option>
          <prop v="0" k="align_dash_pattern"/>
          <prop v="square" k="capstyle"/>
          <prop v="5;2" k="customdash"/>
          <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
          <prop v="MM" k="customdash_unit"/>
          <prop v="0" k="dash_pattern_offset"/>
          <prop v="3x:0,0,0,0,0,0" k="dash_pattern_offset_map_unit_scale"/>
          <prop v="MM" k="dash_pattern_offset_unit"/>
          <prop v="0" k="draw_inside_polygon"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="227,26,28,255" k="line_color"/>
          <prop v="solid" k="line_style"/>
          <prop v="12" k="line_width"/>
          <prop v="MapUnit" k="line_width_unit"/>
          <prop v="0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0" k="ring_filter"/>
          <prop v="0" k="tweak_dash_pattern_on_corners"/>
          <prop v="0" k="use_custom_dash"/>
          <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="1" type="line" alpha="1" force_rhr="0" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" class="SimpleLine" enabled="1" pass="0">
          <Option type="Map">
            <Option name="align_dash_pattern" type="QString" value="0"/>
            <Option name="capstyle" type="QString" value="round"/>
            <Option name="customdash" type="QString" value="5;2"/>
            <Option name="customdash_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="customdash_unit" type="QString" value="MM"/>
            <Option name="dash_pattern_offset" type="QString" value="0"/>
            <Option name="dash_pattern_offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="dash_pattern_offset_unit" type="QString" value="MM"/>
            <Option name="draw_inside_polygon" type="QString" value="0"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="line_color" type="QString" value="0,0,0,255"/>
            <Option name="line_style" type="QString" value="solid"/>
            <Option name="line_width" type="QString" value="14"/>
            <Option name="line_width_unit" type="QString" value="MapUnit"/>
            <Option name="offset" type="QString" value="0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="ring_filter" type="QString" value="0"/>
            <Option name="tweak_dash_pattern_on_corners" type="QString" value="0"/>
            <Option name="use_custom_dash" type="QString" value="0"/>
            <Option name="width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
          </Option>
          <prop v="0" k="align_dash_pattern"/>
          <prop v="round" k="capstyle"/>
          <prop v="5;2" k="customdash"/>
          <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
          <prop v="MM" k="customdash_unit"/>
          <prop v="0" k="dash_pattern_offset"/>
          <prop v="3x:0,0,0,0,0,0" k="dash_pattern_offset_map_unit_scale"/>
          <prop v="MM" k="dash_pattern_offset_unit"/>
          <prop v="0" k="draw_inside_polygon"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="0,0,0,255" k="line_color"/>
          <prop v="solid" k="line_style"/>
          <prop v="14" k="line_width"/>
          <prop v="MapUnit" k="line_width_unit"/>
          <prop v="0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0" k="ring_filter"/>
          <prop v="0" k="tweak_dash_pattern_on_corners"/>
          <prop v="0" k="use_custom_dash"/>
          <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
        <layer locked="0" class="SimpleLine" enabled="1" pass="0">
          <Option type="Map">
            <Option name="align_dash_pattern" type="QString" value="0"/>
            <Option name="capstyle" type="QString" value="round"/>
            <Option name="customdash" type="QString" value="5;2"/>
            <Option name="customdash_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="customdash_unit" type="QString" value="MM"/>
            <Option name="dash_pattern_offset" type="QString" value="0"/>
            <Option name="dash_pattern_offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="dash_pattern_offset_unit" type="QString" value="MM"/>
            <Option name="draw_inside_polygon" type="QString" value="0"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="line_color" type="QString" value="255,160,80,255"/>
            <Option name="line_style" type="QString" value="solid"/>
            <Option name="line_width" type="QString" value="12"/>
            <Option name="line_width_unit" type="QString" value="MapUnit"/>
            <Option name="offset" type="QString" value="0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="ring_filter" type="QString" value="0"/>
            <Option name="tweak_dash_pattern_on_corners" type="QString" value="0"/>
            <Option name="use_custom_dash" type="QString" value="0"/>
            <Option name="width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
          </Option>
          <prop v="0" k="align_dash_pattern"/>
          <prop v="round" k="capstyle"/>
          <prop v="5;2" k="customdash"/>
          <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
          <prop v="MM" k="customdash_unit"/>
          <prop v="0" k="dash_pattern_offset"/>
          <prop v="3x:0,0,0,0,0,0" k="dash_pattern_offset_map_unit_scale"/>
          <prop v="MM" k="dash_pattern_offset_unit"/>
          <prop v="0" k="draw_inside_polygon"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="255,160,80,255" k="line_color"/>
          <prop v="solid" k="line_style"/>
          <prop v="12" k="line_width"/>
          <prop v="MapUnit" k="line_width_unit"/>
          <prop v="0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0" k="ring_filter"/>
          <prop v="0" k="tweak_dash_pattern_on_corners"/>
          <prop v="0" k="use_custom_dash"/>
          <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="2" type="line" alpha="1" force_rhr="0" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" class="SimpleLine" enabled="1" pass="0">
          <Option type="Map">
            <Option name="align_dash_pattern" type="QString" value="0"/>
            <Option name="capstyle" type="QString" value="round"/>
            <Option name="customdash" type="QString" value="5;2"/>
            <Option name="customdash_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="customdash_unit" type="QString" value="MM"/>
            <Option name="dash_pattern_offset" type="QString" value="0"/>
            <Option name="dash_pattern_offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="dash_pattern_offset_unit" type="QString" value="MM"/>
            <Option name="draw_inside_polygon" type="QString" value="0"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="line_color" type="QString" value="0,0,0,255"/>
            <Option name="line_style" type="QString" value="solid"/>
            <Option name="line_width" type="QString" value="3"/>
            <Option name="line_width_unit" type="QString" value="MapUnit"/>
            <Option name="offset" type="QString" value="0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="ring_filter" type="QString" value="0"/>
            <Option name="tweak_dash_pattern_on_corners" type="QString" value="0"/>
            <Option name="use_custom_dash" type="QString" value="0"/>
            <Option name="width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
          </Option>
          <prop v="0" k="align_dash_pattern"/>
          <prop v="round" k="capstyle"/>
          <prop v="5;2" k="customdash"/>
          <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
          <prop v="MM" k="customdash_unit"/>
          <prop v="0" k="dash_pattern_offset"/>
          <prop v="3x:0,0,0,0,0,0" k="dash_pattern_offset_map_unit_scale"/>
          <prop v="MM" k="dash_pattern_offset_unit"/>
          <prop v="0" k="draw_inside_polygon"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="0,0,0,255" k="line_color"/>
          <prop v="solid" k="line_style"/>
          <prop v="3" k="line_width"/>
          <prop v="MapUnit" k="line_width_unit"/>
          <prop v="0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0" k="ring_filter"/>
          <prop v="0" k="tweak_dash_pattern_on_corners"/>
          <prop v="0" k="use_custom_dash"/>
          <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="3" type="line" alpha="1" force_rhr="0" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" class="SimpleLine" enabled="1" pass="0">
          <Option type="Map">
            <Option name="align_dash_pattern" type="QString" value="0"/>
            <Option name="capstyle" type="QString" value="round"/>
            <Option name="customdash" type="QString" value="5;2"/>
            <Option name="customdash_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="customdash_unit" type="QString" value="MM"/>
            <Option name="dash_pattern_offset" type="QString" value="0"/>
            <Option name="dash_pattern_offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="dash_pattern_offset_unit" type="QString" value="MM"/>
            <Option name="draw_inside_polygon" type="QString" value="0"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="line_color" type="QString" value="0,0,0,255"/>
            <Option name="line_style" type="QString" value="solid"/>
            <Option name="line_width" type="QString" value="11"/>
            <Option name="line_width_unit" type="QString" value="MapUnit"/>
            <Option name="offset" type="QString" value="0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="ring_filter" type="QString" value="0"/>
            <Option name="tweak_dash_pattern_on_corners" type="QString" value="0"/>
            <Option name="use_custom_dash" type="QString" value="0"/>
            <Option name="width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
          </Option>
          <prop v="0" k="align_dash_pattern"/>
          <prop v="round" k="capstyle"/>
          <prop v="5;2" k="customdash"/>
          <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
          <prop v="MM" k="customdash_unit"/>
          <prop v="0" k="dash_pattern_offset"/>
          <prop v="3x:0,0,0,0,0,0" k="dash_pattern_offset_map_unit_scale"/>
          <prop v="MM" k="dash_pattern_offset_unit"/>
          <prop v="0" k="draw_inside_polygon"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="0,0,0,255" k="line_color"/>
          <prop v="solid" k="line_style"/>
          <prop v="11" k="line_width"/>
          <prop v="MapUnit" k="line_width_unit"/>
          <prop v="0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0" k="ring_filter"/>
          <prop v="0" k="tweak_dash_pattern_on_corners"/>
          <prop v="0" k="use_custom_dash"/>
          <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
        <layer locked="0" class="SimpleLine" enabled="1" pass="0">
          <Option type="Map">
            <Option name="align_dash_pattern" type="QString" value="0"/>
            <Option name="capstyle" type="QString" value="round"/>
            <Option name="customdash" type="QString" value="5;2"/>
            <Option name="customdash_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="customdash_unit" type="QString" value="MM"/>
            <Option name="dash_pattern_offset" type="QString" value="0"/>
            <Option name="dash_pattern_offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="dash_pattern_offset_unit" type="QString" value="MM"/>
            <Option name="draw_inside_polygon" type="QString" value="0"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="line_color" type="QString" value="255,255,255,255"/>
            <Option name="line_style" type="QString" value="solid"/>
            <Option name="line_width" type="QString" value="9"/>
            <Option name="line_width_unit" type="QString" value="MapUnit"/>
            <Option name="offset" type="QString" value="0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="ring_filter" type="QString" value="0"/>
            <Option name="tweak_dash_pattern_on_corners" type="QString" value="0"/>
            <Option name="use_custom_dash" type="QString" value="0"/>
            <Option name="width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
          </Option>
          <prop v="0" k="align_dash_pattern"/>
          <prop v="round" k="capstyle"/>
          <prop v="5;2" k="customdash"/>
          <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
          <prop v="MM" k="customdash_unit"/>
          <prop v="0" k="dash_pattern_offset"/>
          <prop v="3x:0,0,0,0,0,0" k="dash_pattern_offset_map_unit_scale"/>
          <prop v="MM" k="dash_pattern_offset_unit"/>
          <prop v="0" k="draw_inside_polygon"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="255,255,255,255" k="line_color"/>
          <prop v="solid" k="line_style"/>
          <prop v="9" k="line_width"/>
          <prop v="MapUnit" k="line_width_unit"/>
          <prop v="0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0" k="ring_filter"/>
          <prop v="0" k="tweak_dash_pattern_on_corners"/>
          <prop v="0" k="use_custom_dash"/>
          <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
  </renderer-v2>
  <labeling type="simple">
    <settings calloutType="simple">
      <text-style fontFamily="Noto Sans Display Condensed" blendMode="0" fontStrikeout="0" fontUnderline="0" allowHtml="0" textOpacity="1" multilineHeight="1" fontSizeUnit="MapUnit" fontItalic="0" capitalization="0" fontKerning="1" fontWeight="50" isExpression="0" useSubstitutions="0" fontLetterSpacing="0" textColor="0,0,0,255" textOrientation="horizontal" previewBkgrdColor="255,255,255,255" fieldName="name" fontSizeMapUnitScale="3x:0,0,0,0,0,0" namedStyle="Regular" fontSize="9" fontWordSpacing="0">
        <text-buffer bufferColor="255,255,255,255" bufferNoFill="1" bufferDraw="0" bufferJoinStyle="128" bufferBlendMode="0" bufferSize="1" bufferOpacity="1" bufferSizeUnits="MM" bufferSizeMapUnitScale="3x:0,0,0,0,0,0"/>
        <text-mask maskEnabled="0" maskType="0" maskOpacity="1" maskSizeMapUnitScale="3x:0,0,0,0,0,0" maskJoinStyle="128" maskSize="1.5" maskSizeUnits="MM" maskedSymbolLayers=""/>
        <background shapeSizeUnit="MM" shapeRadiiY="0" shapeBorderWidthUnit="MM" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeRotationType="0" shapeOpacity="1" shapeSizeY="0" shapeSizeType="0" shapeRadiiX="0" shapeBlendMode="0" shapeRadiiUnit="MM" shapeSVGFile="" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeRotation="0" shapeJoinStyle="64" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeOffsetUnit="MM" shapeBorderWidth="0" shapeType="0" shapeDraw="0" shapeBorderColor="128,128,128,255" shapeOffsetY="0" shapeOffsetX="0" shapeFillColor="255,255,255,255" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeSizeX="0">
          <symbol name="markerSymbol" type="marker" alpha="1" force_rhr="0" clip_to_extent="1">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" type="QString" value=""/>
                <Option name="properties"/>
                <Option name="type" type="QString" value="collection"/>
              </Option>
            </data_defined_properties>
            <layer locked="0" class="SimpleMarker" enabled="1" pass="0">
              <Option type="Map">
                <Option name="angle" type="QString" value="0"/>
                <Option name="color" type="QString" value="213,180,60,255"/>
                <Option name="horizontal_anchor_point" type="QString" value="1"/>
                <Option name="joinstyle" type="QString" value="bevel"/>
                <Option name="name" type="QString" value="circle"/>
                <Option name="offset" type="QString" value="0,0"/>
                <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
                <Option name="offset_unit" type="QString" value="MM"/>
                <Option name="outline_color" type="QString" value="35,35,35,255"/>
                <Option name="outline_style" type="QString" value="solid"/>
                <Option name="outline_width" type="QString" value="0"/>
                <Option name="outline_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
                <Option name="outline_width_unit" type="QString" value="MM"/>
                <Option name="scale_method" type="QString" value="diameter"/>
                <Option name="size" type="QString" value="2"/>
                <Option name="size_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
                <Option name="size_unit" type="QString" value="MM"/>
                <Option name="vertical_anchor_point" type="QString" value="1"/>
              </Option>
              <prop v="0" k="angle"/>
              <prop v="213,180,60,255" k="color"/>
              <prop v="1" k="horizontal_anchor_point"/>
              <prop v="bevel" k="joinstyle"/>
              <prop v="circle" k="name"/>
              <prop v="0,0" k="offset"/>
              <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
              <prop v="MM" k="offset_unit"/>
              <prop v="35,35,35,255" k="outline_color"/>
              <prop v="solid" k="outline_style"/>
              <prop v="0" k="outline_width"/>
              <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
              <prop v="MM" k="outline_width_unit"/>
              <prop v="diameter" k="scale_method"/>
              <prop v="2" k="size"/>
              <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
              <prop v="MM" k="size_unit"/>
              <prop v="1" k="vertical_anchor_point"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" type="QString" value=""/>
                  <Option name="properties"/>
                  <Option name="type" type="QString" value="collection"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </background>
        <shadow shadowRadius="1.5" shadowUnder="0" shadowOffsetUnit="MM" shadowBlendMode="6" shadowDraw="0" shadowOffsetGlobal="1" shadowScale="100" shadowRadiusUnit="MM" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowOffsetDist="1" shadowOpacity="0.7" shadowRadiusAlphaOnly="0" shadowOffsetAngle="135" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowColor="0,0,0,255"/>
        <dd_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </dd_properties>
        <substitutions/>
      </text-style>
      <text-format useMaxLineLengthForAutoWrap="1" leftDirectionSymbol="&lt;" decimals="3" addDirectionSymbol="0" autoWrapLength="0" placeDirectionSymbol="0" plussign="0" multilineAlign="0" formatNumbers="0" wrapChar="" reverseDirectionSymbol="0" rightDirectionSymbol=">"/>
      <placement repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" geometryGeneratorEnabled="0" placementFlags="9" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" lineAnchorPercent="0.5" lineAnchorType="0" polygonPlacementFlags="2" centroidWhole="0" centroidInside="0" overrunDistance="0" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" priority="5" geometryGeneratorType="PointGeometry" offsetType="0" offsetUnits="MM" rotationAngle="0" layerType="LineGeometry" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" xOffset="0" yOffset="0" quadOffset="4" geometryGenerator="" maxCurvedCharAngleOut="-25" overrunDistanceUnit="MM" fitInPolygonOnly="0" distUnits="MM" dist="0" distMapUnitScale="3x:0,0,0,0,0,0" placement="3" repeatDistanceUnits="MM" maxCurvedCharAngleIn="25" preserveRotation="1" repeatDistance="0"/>
      <rendering maxNumLabels="2000" obstacle="1" scaleMax="0" scaleVisibility="0" minFeatureSize="0" displayAll="0" fontMinPixelSize="3" obstacleFactor="1" fontLimitPixelSize="0" zIndex="0" mergeLines="0" obstacleType="1" drawLabels="1" scaleMin="0" upsidedownLabels="0" limitNumLabels="0" labelPerPart="0" fontMaxPixelSize="10000"/>
      <dd_properties>
        <Option type="Map">
          <Option name="name" type="QString" value=""/>
          <Option name="properties"/>
          <Option name="type" type="QString" value="collection"/>
        </Option>
      </dd_properties>
      <callout type="simple">
        <Option type="Map">
          <Option name="anchorPoint" type="QString" value="pole_of_inaccessibility"/>
          <Option name="ddProperties" type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
          <Option name="drawToAllParts" type="bool" value="false"/>
          <Option name="enabled" type="QString" value="0"/>
          <Option name="labelAnchorPoint" type="QString" value="point_on_exterior"/>
          <Option name="lineSymbol" type="QString" value="&lt;symbol name=&quot;symbol&quot; type=&quot;line&quot; alpha=&quot;1&quot; force_rhr=&quot;0&quot; clip_to_extent=&quot;1&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option name=&quot;name&quot; type=&quot;QString&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option name=&quot;type&quot; type=&quot;QString&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer locked=&quot;0&quot; class=&quot;SimpleLine&quot; enabled=&quot;1&quot; pass=&quot;0&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option name=&quot;align_dash_pattern&quot; type=&quot;QString&quot; value=&quot;0&quot;/>&lt;Option name=&quot;capstyle&quot; type=&quot;QString&quot; value=&quot;square&quot;/>&lt;Option name=&quot;customdash&quot; type=&quot;QString&quot; value=&quot;5;2&quot;/>&lt;Option name=&quot;customdash_map_unit_scale&quot; type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option name=&quot;customdash_unit&quot; type=&quot;QString&quot; value=&quot;MM&quot;/>&lt;Option name=&quot;dash_pattern_offset&quot; type=&quot;QString&quot; value=&quot;0&quot;/>&lt;Option name=&quot;dash_pattern_offset_map_unit_scale&quot; type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option name=&quot;dash_pattern_offset_unit&quot; type=&quot;QString&quot; value=&quot;MM&quot;/>&lt;Option name=&quot;draw_inside_polygon&quot; type=&quot;QString&quot; value=&quot;0&quot;/>&lt;Option name=&quot;joinstyle&quot; type=&quot;QString&quot; value=&quot;bevel&quot;/>&lt;Option name=&quot;line_color&quot; type=&quot;QString&quot; value=&quot;60,60,60,255&quot;/>&lt;Option name=&quot;line_style&quot; type=&quot;QString&quot; value=&quot;solid&quot;/>&lt;Option name=&quot;line_width&quot; type=&quot;QString&quot; value=&quot;0.3&quot;/>&lt;Option name=&quot;line_width_unit&quot; type=&quot;QString&quot; value=&quot;MM&quot;/>&lt;Option name=&quot;offset&quot; type=&quot;QString&quot; value=&quot;0&quot;/>&lt;Option name=&quot;offset_map_unit_scale&quot; type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option name=&quot;offset_unit&quot; type=&quot;QString&quot; value=&quot;MM&quot;/>&lt;Option name=&quot;ring_filter&quot; type=&quot;QString&quot; value=&quot;0&quot;/>&lt;Option name=&quot;tweak_dash_pattern_on_corners&quot; type=&quot;QString&quot; value=&quot;0&quot;/>&lt;Option name=&quot;use_custom_dash&quot; type=&quot;QString&quot; value=&quot;0&quot;/>&lt;Option name=&quot;width_map_unit_scale&quot; type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;/Option>&lt;prop v=&quot;0&quot; k=&quot;align_dash_pattern&quot;/>&lt;prop v=&quot;square&quot; k=&quot;capstyle&quot;/>&lt;prop v=&quot;5;2&quot; k=&quot;customdash&quot;/>&lt;prop v=&quot;3x:0,0,0,0,0,0&quot; k=&quot;customdash_map_unit_scale&quot;/>&lt;prop v=&quot;MM&quot; k=&quot;customdash_unit&quot;/>&lt;prop v=&quot;0&quot; k=&quot;dash_pattern_offset&quot;/>&lt;prop v=&quot;3x:0,0,0,0,0,0&quot; k=&quot;dash_pattern_offset_map_unit_scale&quot;/>&lt;prop v=&quot;MM&quot; k=&quot;dash_pattern_offset_unit&quot;/>&lt;prop v=&quot;0&quot; k=&quot;draw_inside_polygon&quot;/>&lt;prop v=&quot;bevel&quot; k=&quot;joinstyle&quot;/>&lt;prop v=&quot;60,60,60,255&quot; k=&quot;line_color&quot;/>&lt;prop v=&quot;solid&quot; k=&quot;line_style&quot;/>&lt;prop v=&quot;0.3&quot; k=&quot;line_width&quot;/>&lt;prop v=&quot;MM&quot; k=&quot;line_width_unit&quot;/>&lt;prop v=&quot;0&quot; k=&quot;offset&quot;/>&lt;prop v=&quot;3x:0,0,0,0,0,0&quot; k=&quot;offset_map_unit_scale&quot;/>&lt;prop v=&quot;MM&quot; k=&quot;offset_unit&quot;/>&lt;prop v=&quot;0&quot; k=&quot;ring_filter&quot;/>&lt;prop v=&quot;0&quot; k=&quot;tweak_dash_pattern_on_corners&quot;/>&lt;prop v=&quot;0&quot; k=&quot;use_custom_dash&quot;/>&lt;prop v=&quot;3x:0,0,0,0,0,0&quot; k=&quot;width_map_unit_scale&quot;/>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option name=&quot;name&quot; type=&quot;QString&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option name=&quot;type&quot; type=&quot;QString&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>"/>
          <Option name="minLength" type="double" value="0"/>
          <Option name="minLengthMapUnitScale" type="QString" value="3x:0,0,0,0,0,0"/>
          <Option name="minLengthUnit" type="QString" value="MM"/>
          <Option name="offsetFromAnchor" type="double" value="0"/>
          <Option name="offsetFromAnchorMapUnitScale" type="QString" value="3x:0,0,0,0,0,0"/>
          <Option name="offsetFromAnchorUnit" type="QString" value="MM"/>
          <Option name="offsetFromLabel" type="double" value="0"/>
          <Option name="offsetFromLabelMapUnitScale" type="QString" value="3x:0,0,0,0,0,0"/>
          <Option name="offsetFromLabelUnit" type="QString" value="MM"/>
        </Option>
      </callout>
    </settings>
  </labeling>
  <customproperties>
    <property key="dualview/previewExpressions" value="&quot;name&quot;"/>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory sizeScale="3x:0,0,0,0,0,0" labelPlacementMethod="XHeight" diagramOrientation="Up" lineSizeScale="3x:0,0,0,0,0,0" opacity="1" backgroundColor="#ffffff" showAxis="1" penWidth="0" width="15" minScaleDenominator="0" penColor="#000000" maxScaleDenominator="1e+08" minimumSize="0" backgroundAlpha="255" barWidth="5" enabled="0" rotationOffset="270" spacingUnitScale="3x:0,0,0,0,0,0" sizeType="MM" penAlpha="255" spacing="5" height="15" scaleBasedVisibility="0" lineSizeType="MM" spacingUnit="MM" direction="0" scaleDependency="Area">
      <fontProperties style="" description="Sans,10,-1,5,50,0,0,0,0,0"/>
      <attribute field="" color="#000000" label=""/>
      <axisSymbol>
        <symbol name="" type="line" alpha="1" force_rhr="0" clip_to_extent="1">
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
          <layer locked="0" class="SimpleLine" enabled="1" pass="0">
            <Option type="Map">
              <Option name="align_dash_pattern" type="QString" value="0"/>
              <Option name="capstyle" type="QString" value="square"/>
              <Option name="customdash" type="QString" value="5;2"/>
              <Option name="customdash_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="customdash_unit" type="QString" value="MM"/>
              <Option name="dash_pattern_offset" type="QString" value="0"/>
              <Option name="dash_pattern_offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="dash_pattern_offset_unit" type="QString" value="MM"/>
              <Option name="draw_inside_polygon" type="QString" value="0"/>
              <Option name="joinstyle" type="QString" value="bevel"/>
              <Option name="line_color" type="QString" value="35,35,35,255"/>
              <Option name="line_style" type="QString" value="solid"/>
              <Option name="line_width" type="QString" value="0.26"/>
              <Option name="line_width_unit" type="QString" value="MM"/>
              <Option name="offset" type="QString" value="0"/>
              <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="offset_unit" type="QString" value="MM"/>
              <Option name="ring_filter" type="QString" value="0"/>
              <Option name="tweak_dash_pattern_on_corners" type="QString" value="0"/>
              <Option name="use_custom_dash" type="QString" value="0"/>
              <Option name="width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            </Option>
            <prop v="0" k="align_dash_pattern"/>
            <prop v="square" k="capstyle"/>
            <prop v="5;2" k="customdash"/>
            <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
            <prop v="MM" k="customdash_unit"/>
            <prop v="0" k="dash_pattern_offset"/>
            <prop v="3x:0,0,0,0,0,0" k="dash_pattern_offset_map_unit_scale"/>
            <prop v="MM" k="dash_pattern_offset_unit"/>
            <prop v="0" k="draw_inside_polygon"/>
            <prop v="bevel" k="joinstyle"/>
            <prop v="35,35,35,255" k="line_color"/>
            <prop v="solid" k="line_style"/>
            <prop v="0.26" k="line_width"/>
            <prop v="MM" k="line_width_unit"/>
            <prop v="0" k="offset"/>
            <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
            <prop v="MM" k="offset_unit"/>
            <prop v="0" k="ring_filter"/>
            <prop v="0" k="tweak_dash_pattern_on_corners"/>
            <prop v="0" k="use_custom_dash"/>
            <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" type="QString" value=""/>
                <Option name="properties"/>
                <Option name="type" type="QString" value="collection"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings dist="0" placement="2" linePlacementFlags="18" obstacle="0" priority="0" showAll="1" zIndex="0">
    <properties>
      <Option type="Map">
        <Option name="name" type="QString" value=""/>
        <Option name="properties"/>
        <Option name="type" type="QString" value="collection"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend type="default-vector"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field name="fid" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="full_id" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="osm_id" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="osm_type" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="highway" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="oneway" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="ref" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="surface" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="junction" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="maxspeed" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="source:maxspeed" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="destination" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="name" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="trailblazed" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="trailblazed:mtb" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="tracktype" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="lit" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="bicycle" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="foot" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="lanes" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="motor_vehicle" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="oneway:bicycle" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="zone:maxspeed" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="trailblazed:bicycle" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="bridge" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="cycleway:both" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="layer" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="tunnel" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="maxheight" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="service" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="access" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="name:etymology:wikidata" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="width" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="maxaxleload" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="maxweight" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="maxwidth" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="cycleway:right" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="vehicle" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="ref:FR:FANTOIR" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="cycleway" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="incline" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="sidewalk" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="smoothness" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="mtb:scale" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="wheelchair" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="crossing" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="footway" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="covered" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="toll" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="restriction" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="trail_visibility" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="sac_scale" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="snowplowing:category" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="horse" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="sloped_curb" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="tactile_paving" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="traffic_calming" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" name="" field="fid"/>
    <alias index="1" name="" field="full_id"/>
    <alias index="2" name="" field="osm_id"/>
    <alias index="3" name="" field="osm_type"/>
    <alias index="4" name="" field="highway"/>
    <alias index="5" name="" field="oneway"/>
    <alias index="6" name="" field="ref"/>
    <alias index="7" name="" field="surface"/>
    <alias index="8" name="" field="junction"/>
    <alias index="9" name="" field="maxspeed"/>
    <alias index="10" name="" field="source:maxspeed"/>
    <alias index="11" name="" field="destination"/>
    <alias index="12" name="" field="name"/>
    <alias index="13" name="" field="trailblazed"/>
    <alias index="14" name="" field="trailblazed:mtb"/>
    <alias index="15" name="" field="tracktype"/>
    <alias index="16" name="" field="lit"/>
    <alias index="17" name="" field="bicycle"/>
    <alias index="18" name="" field="foot"/>
    <alias index="19" name="" field="lanes"/>
    <alias index="20" name="" field="motor_vehicle"/>
    <alias index="21" name="" field="oneway:bicycle"/>
    <alias index="22" name="" field="zone:maxspeed"/>
    <alias index="23" name="" field="trailblazed:bicycle"/>
    <alias index="24" name="" field="bridge"/>
    <alias index="25" name="" field="cycleway:both"/>
    <alias index="26" name="" field="layer"/>
    <alias index="27" name="" field="tunnel"/>
    <alias index="28" name="" field="maxheight"/>
    <alias index="29" name="" field="service"/>
    <alias index="30" name="" field="access"/>
    <alias index="31" name="" field="name:etymology:wikidata"/>
    <alias index="32" name="" field="width"/>
    <alias index="33" name="" field="maxaxleload"/>
    <alias index="34" name="" field="maxweight"/>
    <alias index="35" name="" field="maxwidth"/>
    <alias index="36" name="" field="cycleway:right"/>
    <alias index="37" name="" field="vehicle"/>
    <alias index="38" name="" field="ref:FR:FANTOIR"/>
    <alias index="39" name="" field="cycleway"/>
    <alias index="40" name="" field="incline"/>
    <alias index="41" name="" field="sidewalk"/>
    <alias index="42" name="" field="smoothness"/>
    <alias index="43" name="" field="mtb:scale"/>
    <alias index="44" name="" field="wheelchair"/>
    <alias index="45" name="" field="crossing"/>
    <alias index="46" name="" field="footway"/>
    <alias index="47" name="" field="covered"/>
    <alias index="48" name="" field="toll"/>
    <alias index="49" name="" field="restriction"/>
    <alias index="50" name="" field="trail_visibility"/>
    <alias index="51" name="" field="sac_scale"/>
    <alias index="52" name="" field="snowplowing:category"/>
    <alias index="53" name="" field="horse"/>
    <alias index="54" name="" field="sloped_curb"/>
    <alias index="55" name="" field="tactile_paving"/>
    <alias index="56" name="" field="traffic_calming"/>
  </aliases>
  <defaults>
    <default expression="" field="fid" applyOnUpdate="0"/>
    <default expression="" field="full_id" applyOnUpdate="0"/>
    <default expression="" field="osm_id" applyOnUpdate="0"/>
    <default expression="" field="osm_type" applyOnUpdate="0"/>
    <default expression="" field="highway" applyOnUpdate="0"/>
    <default expression="" field="oneway" applyOnUpdate="0"/>
    <default expression="" field="ref" applyOnUpdate="0"/>
    <default expression="" field="surface" applyOnUpdate="0"/>
    <default expression="" field="junction" applyOnUpdate="0"/>
    <default expression="" field="maxspeed" applyOnUpdate="0"/>
    <default expression="" field="source:maxspeed" applyOnUpdate="0"/>
    <default expression="" field="destination" applyOnUpdate="0"/>
    <default expression="" field="name" applyOnUpdate="0"/>
    <default expression="" field="trailblazed" applyOnUpdate="0"/>
    <default expression="" field="trailblazed:mtb" applyOnUpdate="0"/>
    <default expression="" field="tracktype" applyOnUpdate="0"/>
    <default expression="" field="lit" applyOnUpdate="0"/>
    <default expression="" field="bicycle" applyOnUpdate="0"/>
    <default expression="" field="foot" applyOnUpdate="0"/>
    <default expression="" field="lanes" applyOnUpdate="0"/>
    <default expression="" field="motor_vehicle" applyOnUpdate="0"/>
    <default expression="" field="oneway:bicycle" applyOnUpdate="0"/>
    <default expression="" field="zone:maxspeed" applyOnUpdate="0"/>
    <default expression="" field="trailblazed:bicycle" applyOnUpdate="0"/>
    <default expression="" field="bridge" applyOnUpdate="0"/>
    <default expression="" field="cycleway:both" applyOnUpdate="0"/>
    <default expression="" field="layer" applyOnUpdate="0"/>
    <default expression="" field="tunnel" applyOnUpdate="0"/>
    <default expression="" field="maxheight" applyOnUpdate="0"/>
    <default expression="" field="service" applyOnUpdate="0"/>
    <default expression="" field="access" applyOnUpdate="0"/>
    <default expression="" field="name:etymology:wikidata" applyOnUpdate="0"/>
    <default expression="" field="width" applyOnUpdate="0"/>
    <default expression="" field="maxaxleload" applyOnUpdate="0"/>
    <default expression="" field="maxweight" applyOnUpdate="0"/>
    <default expression="" field="maxwidth" applyOnUpdate="0"/>
    <default expression="" field="cycleway:right" applyOnUpdate="0"/>
    <default expression="" field="vehicle" applyOnUpdate="0"/>
    <default expression="" field="ref:FR:FANTOIR" applyOnUpdate="0"/>
    <default expression="" field="cycleway" applyOnUpdate="0"/>
    <default expression="" field="incline" applyOnUpdate="0"/>
    <default expression="" field="sidewalk" applyOnUpdate="0"/>
    <default expression="" field="smoothness" applyOnUpdate="0"/>
    <default expression="" field="mtb:scale" applyOnUpdate="0"/>
    <default expression="" field="wheelchair" applyOnUpdate="0"/>
    <default expression="" field="crossing" applyOnUpdate="0"/>
    <default expression="" field="footway" applyOnUpdate="0"/>
    <default expression="" field="covered" applyOnUpdate="0"/>
    <default expression="" field="toll" applyOnUpdate="0"/>
    <default expression="" field="restriction" applyOnUpdate="0"/>
    <default expression="" field="trail_visibility" applyOnUpdate="0"/>
    <default expression="" field="sac_scale" applyOnUpdate="0"/>
    <default expression="" field="snowplowing:category" applyOnUpdate="0"/>
    <default expression="" field="horse" applyOnUpdate="0"/>
    <default expression="" field="sloped_curb" applyOnUpdate="0"/>
    <default expression="" field="tactile_paving" applyOnUpdate="0"/>
    <default expression="" field="traffic_calming" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint field="fid" notnull_strength="1" unique_strength="1" constraints="3" exp_strength="0"/>
    <constraint field="full_id" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="osm_id" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="osm_type" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="highway" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="oneway" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="ref" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="surface" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="junction" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="maxspeed" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="source:maxspeed" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="destination" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="name" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="trailblazed" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="trailblazed:mtb" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="tracktype" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="lit" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="bicycle" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="foot" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="lanes" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="motor_vehicle" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="oneway:bicycle" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="zone:maxspeed" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="trailblazed:bicycle" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="bridge" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="cycleway:both" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="layer" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="tunnel" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="maxheight" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="service" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="access" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="name:etymology:wikidata" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="width" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="maxaxleload" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="maxweight" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="maxwidth" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="cycleway:right" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="vehicle" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="ref:FR:FANTOIR" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="cycleway" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="incline" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="sidewalk" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="smoothness" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="mtb:scale" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="wheelchair" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="crossing" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="footway" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="covered" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="toll" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="restriction" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="trail_visibility" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="sac_scale" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="snowplowing:category" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="horse" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="sloped_curb" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="tactile_paving" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
    <constraint field="traffic_calming" notnull_strength="0" unique_strength="0" constraints="0" exp_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" field="fid" exp=""/>
    <constraint desc="" field="full_id" exp=""/>
    <constraint desc="" field="osm_id" exp=""/>
    <constraint desc="" field="osm_type" exp=""/>
    <constraint desc="" field="highway" exp=""/>
    <constraint desc="" field="oneway" exp=""/>
    <constraint desc="" field="ref" exp=""/>
    <constraint desc="" field="surface" exp=""/>
    <constraint desc="" field="junction" exp=""/>
    <constraint desc="" field="maxspeed" exp=""/>
    <constraint desc="" field="source:maxspeed" exp=""/>
    <constraint desc="" field="destination" exp=""/>
    <constraint desc="" field="name" exp=""/>
    <constraint desc="" field="trailblazed" exp=""/>
    <constraint desc="" field="trailblazed:mtb" exp=""/>
    <constraint desc="" field="tracktype" exp=""/>
    <constraint desc="" field="lit" exp=""/>
    <constraint desc="" field="bicycle" exp=""/>
    <constraint desc="" field="foot" exp=""/>
    <constraint desc="" field="lanes" exp=""/>
    <constraint desc="" field="motor_vehicle" exp=""/>
    <constraint desc="" field="oneway:bicycle" exp=""/>
    <constraint desc="" field="zone:maxspeed" exp=""/>
    <constraint desc="" field="trailblazed:bicycle" exp=""/>
    <constraint desc="" field="bridge" exp=""/>
    <constraint desc="" field="cycleway:both" exp=""/>
    <constraint desc="" field="layer" exp=""/>
    <constraint desc="" field="tunnel" exp=""/>
    <constraint desc="" field="maxheight" exp=""/>
    <constraint desc="" field="service" exp=""/>
    <constraint desc="" field="access" exp=""/>
    <constraint desc="" field="name:etymology:wikidata" exp=""/>
    <constraint desc="" field="width" exp=""/>
    <constraint desc="" field="maxaxleload" exp=""/>
    <constraint desc="" field="maxweight" exp=""/>
    <constraint desc="" field="maxwidth" exp=""/>
    <constraint desc="" field="cycleway:right" exp=""/>
    <constraint desc="" field="vehicle" exp=""/>
    <constraint desc="" field="ref:FR:FANTOIR" exp=""/>
    <constraint desc="" field="cycleway" exp=""/>
    <constraint desc="" field="incline" exp=""/>
    <constraint desc="" field="sidewalk" exp=""/>
    <constraint desc="" field="smoothness" exp=""/>
    <constraint desc="" field="mtb:scale" exp=""/>
    <constraint desc="" field="wheelchair" exp=""/>
    <constraint desc="" field="crossing" exp=""/>
    <constraint desc="" field="footway" exp=""/>
    <constraint desc="" field="covered" exp=""/>
    <constraint desc="" field="toll" exp=""/>
    <constraint desc="" field="restriction" exp=""/>
    <constraint desc="" field="trail_visibility" exp=""/>
    <constraint desc="" field="sac_scale" exp=""/>
    <constraint desc="" field="snowplowing:category" exp=""/>
    <constraint desc="" field="horse" exp=""/>
    <constraint desc="" field="sloped_curb" exp=""/>
    <constraint desc="" field="tactile_paving" exp=""/>
    <constraint desc="" field="traffic_calming" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig sortExpression="&quot;fid&quot;" sortOrder="0" actionWidgetStyle="dropDown">
    <columns>
      <column name="fid" type="field" width="-1" hidden="0"/>
      <column name="full_id" type="field" width="-1" hidden="0"/>
      <column name="osm_id" type="field" width="-1" hidden="0"/>
      <column name="osm_type" type="field" width="-1" hidden="0"/>
      <column name="highway" type="field" width="-1" hidden="0"/>
      <column name="oneway" type="field" width="-1" hidden="0"/>
      <column name="ref" type="field" width="-1" hidden="0"/>
      <column name="surface" type="field" width="-1" hidden="0"/>
      <column name="junction" type="field" width="-1" hidden="0"/>
      <column name="maxspeed" type="field" width="-1" hidden="0"/>
      <column name="source:maxspeed" type="field" width="-1" hidden="0"/>
      <column name="destination" type="field" width="-1" hidden="0"/>
      <column name="name" type="field" width="-1" hidden="0"/>
      <column name="trailblazed" type="field" width="-1" hidden="0"/>
      <column name="trailblazed:mtb" type="field" width="-1" hidden="0"/>
      <column name="tracktype" type="field" width="-1" hidden="0"/>
      <column name="lit" type="field" width="-1" hidden="0"/>
      <column name="bicycle" type="field" width="-1" hidden="0"/>
      <column name="foot" type="field" width="-1" hidden="0"/>
      <column name="lanes" type="field" width="-1" hidden="0"/>
      <column name="motor_vehicle" type="field" width="-1" hidden="0"/>
      <column name="oneway:bicycle" type="field" width="-1" hidden="0"/>
      <column name="zone:maxspeed" type="field" width="-1" hidden="0"/>
      <column name="trailblazed:bicycle" type="field" width="-1" hidden="0"/>
      <column name="bridge" type="field" width="-1" hidden="0"/>
      <column name="cycleway:both" type="field" width="-1" hidden="0"/>
      <column name="layer" type="field" width="-1" hidden="0"/>
      <column name="maxheight" type="field" width="-1" hidden="0"/>
      <column name="service" type="field" width="-1" hidden="0"/>
      <column name="access" type="field" width="-1" hidden="0"/>
      <column name="name:etymology:wikidata" type="field" width="-1" hidden="0"/>
      <column name="width" type="field" width="-1" hidden="0"/>
      <column name="maxaxleload" type="field" width="-1" hidden="0"/>
      <column name="maxweight" type="field" width="-1" hidden="0"/>
      <column name="maxwidth" type="field" width="-1" hidden="0"/>
      <column name="vehicle" type="field" width="-1" hidden="0"/>
      <column name="ref:FR:FANTOIR" type="field" width="-1" hidden="0"/>
      <column name="cycleway" type="field" width="-1" hidden="0"/>
      <column name="cycleway:right" type="field" width="-1" hidden="0"/>
      <column name="incline" type="field" width="-1" hidden="0"/>
      <column name="sidewalk" type="field" width="-1" hidden="0"/>
      <column name="tunnel" type="field" width="-1" hidden="0"/>
      <column name="smoothness" type="field" width="-1" hidden="0"/>
      <column name="mtb:scale" type="field" width="-1" hidden="0"/>
      <column name="wheelchair" type="field" width="-1" hidden="0"/>
      <column name="crossing" type="field" width="-1" hidden="0"/>
      <column name="footway" type="field" width="-1" hidden="0"/>
      <column name="covered" type="field" width="-1" hidden="0"/>
      <column name="toll" type="field" width="-1" hidden="0"/>
      <column name="restriction" type="field" width="-1" hidden="0"/>
      <column name="sac_scale" type="field" width="-1" hidden="0"/>
      <column name="trail_visibility" type="field" width="-1" hidden="0"/>
      <column name="snowplowing:category" type="field" width="-1" hidden="0"/>
      <column name="horse" type="field" width="-1" hidden="0"/>
      <column name="sloped_curb" type="field" width="-1" hidden="0"/>
      <column name="tactile_paving" type="field" width="-1" hidden="0"/>
      <column name="traffic_calming" type="field" width="-1" hidden="0"/>
      <column type="actions" width="-1" hidden="1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="access" editable="1"/>
    <field name="bicycle" editable="1"/>
    <field name="bridge" editable="1"/>
    <field name="covered" editable="1"/>
    <field name="crossing" editable="1"/>
    <field name="cycleway" editable="1"/>
    <field name="cycleway:both" editable="1"/>
    <field name="cycleway:right" editable="1"/>
    <field name="destination" editable="1"/>
    <field name="fid" editable="1"/>
    <field name="foot" editable="1"/>
    <field name="footway" editable="1"/>
    <field name="full_id" editable="1"/>
    <field name="highway" editable="1"/>
    <field name="horse" editable="1"/>
    <field name="incline" editable="1"/>
    <field name="junction" editable="1"/>
    <field name="lanes" editable="1"/>
    <field name="layer" editable="1"/>
    <field name="lit" editable="1"/>
    <field name="maxaxleload" editable="1"/>
    <field name="maxheight" editable="1"/>
    <field name="maxspeed" editable="1"/>
    <field name="maxweight" editable="1"/>
    <field name="maxwidth" editable="1"/>
    <field name="motor_vehicle" editable="1"/>
    <field name="mtb:scale" editable="1"/>
    <field name="name" editable="1"/>
    <field name="name:etymology:wikidata" editable="1"/>
    <field name="oneway" editable="1"/>
    <field name="oneway:bicycle" editable="1"/>
    <field name="osm_id" editable="1"/>
    <field name="osm_type" editable="1"/>
    <field name="ref" editable="1"/>
    <field name="ref:FR:FANTOIR" editable="1"/>
    <field name="restriction" editable="1"/>
    <field name="sac_scale" editable="1"/>
    <field name="service" editable="1"/>
    <field name="sidewalk" editable="1"/>
    <field name="sloped_curb" editable="1"/>
    <field name="smoothness" editable="1"/>
    <field name="snowplowing:category" editable="1"/>
    <field name="source:maxspeed" editable="1"/>
    <field name="surface" editable="1"/>
    <field name="tactile_paving" editable="1"/>
    <field name="toll" editable="1"/>
    <field name="tracktype" editable="1"/>
    <field name="traffic_calming" editable="1"/>
    <field name="trail_visibility" editable="1"/>
    <field name="trailblazed" editable="1"/>
    <field name="trailblazed:bicycle" editable="1"/>
    <field name="trailblazed:mtb" editable="1"/>
    <field name="tunnel" editable="1"/>
    <field name="vehicle" editable="1"/>
    <field name="wheelchair" editable="1"/>
    <field name="width" editable="1"/>
    <field name="zone:maxspeed" editable="1"/>
  </editable>
  <labelOnTop>
    <field name="access" labelOnTop="0"/>
    <field name="bicycle" labelOnTop="0"/>
    <field name="bridge" labelOnTop="0"/>
    <field name="covered" labelOnTop="0"/>
    <field name="crossing" labelOnTop="0"/>
    <field name="cycleway" labelOnTop="0"/>
    <field name="cycleway:both" labelOnTop="0"/>
    <field name="cycleway:right" labelOnTop="0"/>
    <field name="destination" labelOnTop="0"/>
    <field name="fid" labelOnTop="0"/>
    <field name="foot" labelOnTop="0"/>
    <field name="footway" labelOnTop="0"/>
    <field name="full_id" labelOnTop="0"/>
    <field name="highway" labelOnTop="0"/>
    <field name="horse" labelOnTop="0"/>
    <field name="incline" labelOnTop="0"/>
    <field name="junction" labelOnTop="0"/>
    <field name="lanes" labelOnTop="0"/>
    <field name="layer" labelOnTop="0"/>
    <field name="lit" labelOnTop="0"/>
    <field name="maxaxleload" labelOnTop="0"/>
    <field name="maxheight" labelOnTop="0"/>
    <field name="maxspeed" labelOnTop="0"/>
    <field name="maxweight" labelOnTop="0"/>
    <field name="maxwidth" labelOnTop="0"/>
    <field name="motor_vehicle" labelOnTop="0"/>
    <field name="mtb:scale" labelOnTop="0"/>
    <field name="name" labelOnTop="0"/>
    <field name="name:etymology:wikidata" labelOnTop="0"/>
    <field name="oneway" labelOnTop="0"/>
    <field name="oneway:bicycle" labelOnTop="0"/>
    <field name="osm_id" labelOnTop="0"/>
    <field name="osm_type" labelOnTop="0"/>
    <field name="ref" labelOnTop="0"/>
    <field name="ref:FR:FANTOIR" labelOnTop="0"/>
    <field name="restriction" labelOnTop="0"/>
    <field name="sac_scale" labelOnTop="0"/>
    <field name="service" labelOnTop="0"/>
    <field name="sidewalk" labelOnTop="0"/>
    <field name="sloped_curb" labelOnTop="0"/>
    <field name="smoothness" labelOnTop="0"/>
    <field name="snowplowing:category" labelOnTop="0"/>
    <field name="source:maxspeed" labelOnTop="0"/>
    <field name="surface" labelOnTop="0"/>
    <field name="tactile_paving" labelOnTop="0"/>
    <field name="toll" labelOnTop="0"/>
    <field name="tracktype" labelOnTop="0"/>
    <field name="traffic_calming" labelOnTop="0"/>
    <field name="trail_visibility" labelOnTop="0"/>
    <field name="trailblazed" labelOnTop="0"/>
    <field name="trailblazed:bicycle" labelOnTop="0"/>
    <field name="trailblazed:mtb" labelOnTop="0"/>
    <field name="tunnel" labelOnTop="0"/>
    <field name="vehicle" labelOnTop="0"/>
    <field name="wheelchair" labelOnTop="0"/>
    <field name="width" labelOnTop="0"/>
    <field name="zone:maxspeed" labelOnTop="0"/>
  </labelOnTop>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"name"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>1</layerGeometryType>
</qgis>
